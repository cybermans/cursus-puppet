#!/bin/bash

yum install -y screen puppet-server
cp /vagrant/scripts/50-vagrant-mount.rules /etc/udev/rules.d/
systemctl start puppetmaster
systemctl disable puppetmaster